package com.mkravchenko;

import com.mkravchenko.validators.VerifyA;
import com.mkravchenko.validators.Eddr;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.time.LocalDate;

public class MessagePojo implements Serializable {
    @Size(min = 7)
    @VerifyA
    private String name;
    @Eddr
    private String eddr;
    @Min(value = 10)
    private int count;
    private LocalDate createdAt;

    public MessagePojo() {
    }

    public MessagePojo(String name, String eddr, int count, LocalDate createdAt) {
        this.name = name;
        this.eddr = eddr;
        this.count = count;
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEddr() {
        return eddr;
    }

    public void setEddr(String eddr) {
        this.eddr = eddr;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }
}