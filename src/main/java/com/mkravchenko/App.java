package com.mkravchenko;

import com.mkravchenko.util.PropertiesLoader;
import com.mkravchenko.util.PropertiesReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Hello world!
 */
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    public static final int MIN_MESSAGES_NUMBER = 1001;

    public static void main(String[] args) {
        LOGGER.info("The program has started.");
        try {
            var propertiesReader = new PropertiesReader(new PropertiesLoader().loadProperties("app.properties"));
            new MessageBroker(getNumberOfMessages(args[0]), propertiesReader).run();
        } catch (ExceptionInInitializerError | RuntimeException e) {
            LOGGER.error("Something went wrong!", e);
        }
        LOGGER.info("End of programme execution.");
    }

    protected static int getNumberOfMessages(String messages) {
        var messagesNumber = Integer.parseInt(messages);
        if (messagesNumber < MIN_MESSAGES_NUMBER)
            LOGGER.info("{} less MIN_MESSAGES_NUMBER. 1001 messages will be sent", messagesNumber);

        return Math.max(messagesNumber, MIN_MESSAGES_NUMBER);
    }
}
