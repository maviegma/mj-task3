package com.mkravchenko;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mkravchenko.util.PropertiesReader;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.hibernate.validator.HibernateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class MessageBroker {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageBroker.class);
    private static final int MAX_CONNECTIONS = 10;
    public static final String PRODUCER = "Producer";
    public static final String CONSUMER = "Consumer";

    private final PropertiesReader propertiesReader;
    private final int numberOfMessages;
    private final int numberOfProducer;
    private final int numberOfConsumer;
    private final long workTime;
    private final String validCsv;
    private final String invalidCsv;
    private final String queueName;
    private int producerCounter;
    private long runningProducerCounter;
    private long runningConsumerCounter;
    private final ObjectMapper mapper;

    public MessageBroker(int numberOfMessages, PropertiesReader propertiesReader) {
        this.propertiesReader = propertiesReader;
        this.numberOfMessages = numberOfMessages;
        this.validCsv = propertiesReader.get("validFilePath");
        this.invalidCsv = propertiesReader.get("invalidFilePath");
        this.numberOfProducer = Integer.parseInt(propertiesReader.get("numberOfProducer"));
        this.numberOfConsumer = Integer.parseInt(propertiesReader.get("numberOfConsumer"));
        this.workTime = Integer.parseInt(propertiesReader.get("workTime"));
        this.queueName = propertiesReader.get("queue_name");
        this.mapper = createObjectMapper();
    }

    public void run() {
        var connectionFactory = createConnectionFactory();
        LOGGER.info("Create Connection Factory.");
        var producerExecutor = Executors.newFixedThreadPool(numberOfProducer);
        LOGGER.info("Producer executor has been created.");
        var consumerExecutor = Executors.newFixedThreadPool(numberOfConsumer);
        LOGGER.info("Consumer executor has been created.");

        try (var validPrinter = new CSVPrinter(new FileWriter(validCsv, true), CSVFormat.DEFAULT);
             var inValidPrinter = new CSVPrinter(new FileWriter(invalidCsv, true), CSVFormat.DEFAULT);
             var validatorFactory = Validation.byProvider(HibernateValidator.class)
                     .configure().defaultLocale(Locale.US).buildValidatorFactory()) {

            long startTimeProducer = System.currentTimeMillis();
           Stream.generate(() -> createProducer(connectionFactory))
                    .limit(numberOfProducer)
                    .forEach(producerExecutor::submit);

            var validator = validatorFactory.getValidator();

            long startTimeConsumer = System.currentTimeMillis();
            Stream.generate(() -> createConsumer(connectionFactory, validPrinter, inValidPrinter,validator))
                    .limit(numberOfConsumer)
                    .forEach(consumerExecutor::submit);

            shutdownAndAwaitTermination(producerExecutor, PRODUCER);
            long endTimeProducer = System.currentTimeMillis();
            shutdownAndAwaitTermination(consumerExecutor, CONSUMER);
            long endTimeConsumer = System.currentTimeMillis();

            LOGGER.info("AvgNumberOfSending: {}", numberOfMessages * 1000L / (endTimeProducer - startTimeProducer));
            LOGGER.info("AvgNumberOfReading: {}", numberOfMessages * 1000L / (endTimeConsumer - startTimeConsumer));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        connectionFactory.clear();
    }

    protected Producer createProducer(PooledConnectionFactory connectionFactory){
        runningProducerCounter++;
        return new Producer(connectionFactory, calculateMessageForSingleProducer(),
                numberOfProducer, numberOfConsumer, workTime, mapper, queueName);
    }

    private Consumer createConsumer(PooledConnectionFactory connectionFactory, CSVPrinter validPrinter,
                                    CSVPrinter invalidPrinter, Validator validator){
        runningConsumerCounter++;
        return new Consumer(connectionFactory,
                validPrinter, invalidPrinter, validator, mapper, queueName);
    }

    private void shutdownAndAwaitTermination(ExecutorService executor, String executorName) {
        executor.shutdown();
        long waitingTime = PRODUCER.equals(executorName) ? (long) (workTime * 1.2) : workTime * 4;
        try {
            if (!executor.awaitTermination(waitingTime, TimeUnit.SECONDS)) {
                LOGGER.error("{}'s time is up... not all tasks have been completed.", executorName);
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            LOGGER.debug("The {} executor has been interrupted", executorName, e);
            executor.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    private static ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        objectMapper.setDateFormat(df);
        return objectMapper;
    }

    private PooledConnectionFactory createConnectionFactory() {
        final PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(createActiveMQConnectionFactory());
        pooledConnectionFactory.setMaxConnections(MAX_CONNECTIONS);

        return pooledConnectionFactory;
    }

    private ActiveMQConnectionFactory createActiveMQConnectionFactory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(propertiesReader.get("url"));
        connectionFactory.setTrustedPackages(List.of("com.mkravchenko"));
        connectionFactory.setUserName(propertiesReader.get("userName"));
        connectionFactory.setPassword(propertiesReader.get("password"));
        LOGGER.info("Created a connection factory.");

        return connectionFactory;
    }

    private int calculateMessageForSingleProducer() {
        var number = numberOfMessages / numberOfProducer;

        return producerCounter++ == 0 ? number + numberOfMessages % numberOfProducer : number;
    }

    public long getRunningConsumerCounter() {
        return runningConsumerCounter;
    }

    public long getRunningProducerCounter() {
        return runningProducerCounter;
    }
}
