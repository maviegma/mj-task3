package com.mkravchenko.validators;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EddrImpl.class)
public @interface Eddr {

    String message() default "eddr not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
