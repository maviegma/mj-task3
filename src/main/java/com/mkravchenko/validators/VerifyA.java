package com.mkravchenko.validators;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = VerifyAImpl.class)
public @interface VerifyA {

    String message() default "name must contains literal 'a'";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
