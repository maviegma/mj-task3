package com.mkravchenko.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Objects;

public class EddrImpl implements ConstraintValidator<Eddr, String> {
    @Override
    public void initialize(Eddr constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String eddr, ConstraintValidatorContext constraintValidatorContext) {
        return isValidDate(eddr) && isValidEddr(eddr);
    }

    private boolean isValidEddr(String eddr) {
        if (!isStringEddr(eddr))
            return false;
        int[] ints = createIntArrayFromString(Objects.requireNonNull(eddr));

        return calculateLastNumberEddr(ints) == ints[ints.length - 1];
    }

    protected   boolean isStringEddr(String eddr) {
        return eddr.matches("\\d{8}\\W?\\d{5}");
    }

    protected boolean isValidDate(String eddr) {
        try {
            return isDateInRange(LocalDate.parse(eddr.substring(0, 8), DateTimeFormatter.BASIC_ISO_DATE));
        } catch (DateTimeParseException ex) {
            return false;
        }
    }

    protected boolean isDateInRange(LocalDate date) {
        return !date.isBefore(LocalDate.ofEpochDay(0)) && !date.isAfter(LocalDate.now());
    }

    private static int calculateLastNumberEddr(int[] ints) {
        int[] code = {7, 3, 1};
        int sum = 0;
        for (int i = 0; i < ints.length - 1; i++) {
            if (i % 3 == 0) sum += ints[i] * code[0];
            if (i % 3 == 1) sum += ints[i] * code[1];
            if (i % 3 == 2) sum += ints[i] * code[2];
        }
        return sum % 10;
    }

    private static int[] createIntArrayFromString(String eddr) {
        return Arrays.stream(eddr.split(""))
                .filter(c -> c.matches("\\d"))
                .mapToInt(Integer::parseInt)
                .toArray();
    }
}
