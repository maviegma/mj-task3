package com.mkravchenko.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Objects;

public class VerifyAImpl implements ConstraintValidator<VerifyA, String> {

    @Override
    public void initialize(VerifyA constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String enteredValue, ConstraintValidatorContext constraintValidatorContext) {
        return Objects.requireNonNull(enteredValue).toLowerCase().contains("a");
    }
}
