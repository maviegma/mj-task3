package com.mkravchenko;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mkravchenko.exeptions.ConsumerFailException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Consumer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
    public static final String POISON_PILL = "poison pill";

    private final PooledConnectionFactory connectionFactory;
    private final CSVPrinter validPrinter;
    private final CSVPrinter invalidPrinter;
    private final Validator validator;
    private final ObjectMapper objectMapper;
    private final String queueName;
    static AtomicInteger count = new AtomicInteger();
    private int counterReceivedMessages;

    public Consumer(PooledConnectionFactory connectionFactory, CSVPrinter validPrinter, CSVPrinter invalidPrinter,
                    Validator validator, ObjectMapper mapper, String queueName) {
        this.connectionFactory = connectionFactory;
        this.validPrinter = validPrinter;
        this.invalidPrinter = invalidPrinter;
        this.validator = validator;
        this.objectMapper = mapper;
        this.queueName = queueName;
    }

    @Override
    public void run() {
        try (Connection connection = connectionFactory.createConnection()) {
            connection.start();
            try (Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)) {
                Destination destination = session.createQueue(queueName);
                try (MessageConsumer consumer = session.createConsumer(destination)) {
                    LOGGER.info("launched");
                    Stream.generate(() -> receiveMessage(consumer))
                            .takeWhile(Objects::nonNull)
                            .filter(TextMessage.class::isInstance)
                            .map(this::getTextFromMessage)
                            .takeWhile(m -> !"poison pill".equals(m))
                            .map(this::createMessagePojo)
                            .forEach(this::writeMessage);

                    LOGGER.info("read {} messages.", counterReceivedMessages);
                }
            }
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeMessage(MessagePojo messagePojo) {
        var constraintViolations = validator.validate(messagePojo);
        if (constraintViolations.isEmpty()) {
            writeToValidCSV(messagePojo);
        } else {
            writeToInvalidCSV(messagePojo, constraintViolations);
        }
        count.incrementAndGet();
    }

    private void writeToValidCSV(MessagePojo messagePojo) {
        try {
            validPrinter.printRecord(messagePojo.getCount(), messagePojo.getName());
            counterReceivedMessages++;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeToInvalidCSV(MessagePojo pojo, Set<ConstraintViolation<MessagePojo>> constraintViolations) {
        try {
            invalidPrinter.printRecord(pojo.getCount(), pojo.getName(),
                    new ErrorContainer(constraintViolations.stream()
                            .map(ConstraintViolation::getMessage)
                            .collect(Collectors.toList())));
            counterReceivedMessages++;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected MessagePojo createMessagePojo(String message) {
        try {
            return objectMapper.readValue(message, MessagePojo.class);
        } catch (JsonProcessingException e) {
            LOGGER.error("JSON not convert to object MessagePojo.");
            throw new ConsumerFailException(e);
        }
    }

    protected String getTextFromMessage(Message message) {
        try {
            var text = ((TextMessage) message).getText();
            if (!POISON_PILL.equals(text) && counterReceivedMessages % 10000 == 0) {
                LOGGER.info("take <<<- message {}", text);
            }
            if (POISON_PILL.equals(text))
                LOGGER.info("take <<<- {}", POISON_PILL);
            return text;
        } catch (JMSException e) {
            LOGGER.error("TextMessage not convert to string.");
            throw new ConsumerFailException(e);
        }
    }

    private Message receiveMessage(MessageConsumer consumer) {
        try {
            return consumer.receive();
        } catch (JMSException e) {
            LOGGER.error("Message not received.");
            throw new ConsumerFailException(e);
        }
    }
}
