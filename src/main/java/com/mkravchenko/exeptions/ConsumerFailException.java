package com.mkravchenko.exeptions;

public class ConsumerFailException extends RuntimeException{
    public ConsumerFailException(Throwable cause) {
        super(cause);
    }
}
