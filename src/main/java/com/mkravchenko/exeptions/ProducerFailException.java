package com.mkravchenko.exeptions;


public class ProducerFailException extends RuntimeException {

    public ProducerFailException(Throwable cause) {
        super(cause);
    }
}
