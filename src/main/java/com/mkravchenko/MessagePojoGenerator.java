package com.mkravchenko;

import com.github.javafaker.Faker;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ThreadLocalRandom;

public class MessagePojoGenerator {

    public static final Faker FAKER = new Faker();

    public MessagePojo createMessagePojo() {
        String count = FAKER.numerify("###");
        MessagePojo message = new MessagePojo();
        message.setName(createMessage());
        message.setCount(Integer.parseInt(count));
        message.setCreatedAt(LocalDate.now());
        message.setEddr(createEddr(count));

        return message;
    }

    protected LocalDate createDate() {
        return LocalDate
                .ofEpochDay(ThreadLocalRandom.current().nextLong(0, LocalDate.now().toEpochDay()));
    }

    protected String createEddr(String count) {
        return String.format("%s-0%s%s",
                createDate().format(DateTimeFormatter.ofPattern("yyyyMMdd")), count, FAKER.numerify("#"));
    }

    protected String createMessage() {
        return FAKER.name().fullName();
    }
}
