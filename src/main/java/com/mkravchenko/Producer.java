package com.mkravchenko;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mkravchenko.exeptions.ProducerFailException;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class Producer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    public static final AtomicInteger COUNT_EXECUTED_PRODUCER = new AtomicInteger();
    private final PooledConnectionFactory connectionFactory;
    private final String queueName;
    private int numberOfMessages;
    private final int numberOfProducer;
    private int numberOfConsumer;
    private final long workTime;
    private final ObjectMapper mapper;
    private int counterSentMessages;

    public Producer(PooledConnectionFactory connectionFactory, int messages, int producers,
                    int consumers, long workTime, ObjectMapper mapper, String queueName) {
        this.connectionFactory = connectionFactory;
        this.numberOfMessages = messages;
        this.workTime = workTime;
        this.numberOfProducer = producers;
        this.numberOfConsumer = consumers;
        this.mapper = mapper;
        this.queueName = queueName;
    }

    @Override
    public void run() {

        try (final Connection producerConnection = connectionFactory.createConnection()) {
            producerConnection.start();
            try (Session session = producerConnection.createSession(false, Session.AUTO_ACKNOWLEDGE)) {
                Destination destination = session.createQueue(queueName);
                try (MessageProducer producer = session.createProducer(destination)) {
                    producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

                    LOGGER.info("launched");

                    var startTime = System.currentTimeMillis();
                    Stream.generate(this::createMessagePojo)
                            .takeWhile(m -> System.currentTimeMillis() - startTime <= workTime * 1000)
                            .takeWhile(m -> counterSentMessages < numberOfMessages)
                            .forEach(message -> sendMessage(session, producer, message));

                    LOGGER.info("sent ->>> {} message", counterSentMessages);

                    incrementExecutedProducer();
                    if (COUNT_EXECUTED_PRODUCER.get() == numberOfProducer) {
                        sendPoison(session, producer);
                    }
                }
            }
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    protected void sendPoison(Session session, MessageProducer producer) {
        for (int i = 0; i < numberOfConsumer; i++) {
            try {
                producer.send(session.createTextMessage("poison pill"));
            } catch (JMSException e) {
                LOGGER.error("Producer can't send poison pill.");
                throw new ProducerFailException(e);
            }
            LOGGER.info("sent ->>> poison pill");
        }
    }

    protected void sendMessage(Session session, MessageProducer producer, MessagePojo message) {
        try {
            var text = mapper.writeValueAsString(message);
            producer.send(session.createTextMessage(text));
            counterSentMessages++;
            if (counterSentMessages % 10000 == 0)
                LOGGER.info("sent ->>> message: {}", text);
        } catch (JsonProcessingException e) {
            LOGGER.error("Producer can't create a message.");
            throw new ProducerFailException(e);
        } catch (JMSException e) {
            LOGGER.error("Producer can't send a message.");
            throw new ProducerFailException(e);
        }
    }

    private MessagePojo createMessagePojo() {
        return new MessagePojoGenerator().createMessagePojo();
    }

    private static void incrementExecutedProducer() {
        COUNT_EXECUTED_PRODUCER.incrementAndGet();
    }

public int getCounterSentMessages() {
    return counterSentMessages;
}

    public void setNumberOfMessages(int numberOfMessages) {
        this.numberOfMessages = numberOfMessages;
    }

    public void setNumberOfConsumer(int numberOfConsumer) {
        this.numberOfConsumer = numberOfConsumer;
    }
}
