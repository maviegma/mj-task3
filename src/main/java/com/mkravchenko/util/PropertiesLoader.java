package com.mkravchenko.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;

public class PropertiesLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesLoader.class);
    private final Properties properties;

    public PropertiesLoader() {
        properties = new Properties();
    }

    public  Properties loadProperties(String file) {
        try (var reader = new InputStreamReader(
                Objects.requireNonNull(PropertiesLoader.class.getClassLoader().getResourceAsStream(file)))) {
            properties.load(reader);
            LOGGER.info("Properties loaded from file {}", file);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        return properties;
    }
}
