package com.mkravchenko.util;

import java.util.Properties;

public class PropertiesReader {
    private final Properties properties;

    public PropertiesReader(Properties properties) {
        this.properties = properties;
    }

    public String get(String key) {
        var property = properties.getProperty(key);
        if (property == null)
            throw new IllegalArgumentException("The property '" + key + "' doesn't exist.");
        return property;
    }
}
