package com.mkravchenko;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class AppTest {

    @Test
    void testIfValueIsGreaterDefault() {
        String value = "10000";
        assertThat(App.getNumberOfMessages(value)).isEqualTo(10000);
    }

    @Test
    void testIfValueIsLessOrEqualsDefault() {
        assertThat(App.getNumberOfMessages("1000")).isEqualTo(1001);
        assertThat(App.getNumberOfMessages("100")).isEqualTo(1001);
    }
}