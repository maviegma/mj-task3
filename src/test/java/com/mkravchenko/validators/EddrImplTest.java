package com.mkravchenko.validators;

import jakarta.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class EddrImplTest {
    @Mock
    ConstraintValidatorContext constraintValidatorContext;
    @Spy
    EddrImpl validator;

    @Test
    void testIfCorrectEddr() {

        assertThat(validator.isValid("19970209-06759", constraintValidatorContext)).isTrue();
    }

    @Test
    void testIfNotCorrectEddr() {
        assertThat(validator.isValid("20221124-09674", constraintValidatorContext)).isFalse();
    }

    @Test
    void testIfNotCorrectStringEddr() {
        assertThat(validator.isStringEddr("19970209--06759")).isFalse();
    }

    @Test
    void testDateFromFuture() {
        assertThat(validator.isDateInRange(LocalDate.of(2025, 4, 12))).isFalse();
    }

    @Test
    void testDateFromPast() {
        assertThat(validator.isDateInRange(LocalDate.of(1934, 4, 12))).isFalse();
    }

    @Test
    void testNotCorrectDate() {
        assertThat(validator.isValidDate("20200230")).isFalse();
    }


}