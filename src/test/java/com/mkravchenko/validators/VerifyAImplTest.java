package com.mkravchenko.validators;

import jakarta.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class VerifyAImplTest {
    @Mock
    ConstraintValidatorContext constraintValidatorContext;
    @Spy
    VerifyAImpl validator;
    @Test
    void testStringContainsLiteralA(){
        assertThat(validator.isValid("Black ball.", constraintValidatorContext)).isTrue();
    }

    @Test
    void testStringNotContainsLiteralA(){
        assertThat(validator.isValid("Blue sky.", constraintValidatorContext)).isFalse();
    }

}