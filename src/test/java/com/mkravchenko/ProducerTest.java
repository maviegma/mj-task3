package com.mkravchenko;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mkravchenko.exeptions.ProducerFailException;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.*;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProducerTest {
    @Mock
    PooledConnectionFactory connectionFactory;
    @Mock
    private Connection connection;
    @Mock
    private Session session;
    @Mock
    private MessageProducer messageProducer;
    @Mock
    private Queue queue;
    @Mock
    private ObjectMapper mapper;
    Producer producer;

    @BeforeEach
    public void init() throws JMSException {
        lenient().when(connectionFactory.createConnection()).thenReturn(connection);
        lenient().when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        lenient().when(session.createProducer(any(Destination.class))).thenReturn(messageProducer);
        lenient().when(session.createQueue(anyString())).thenReturn(queue);
        producer = new Producer(connectionFactory,
                1, 1, 1, 1, mapper, "queue");
    }

    @Test
    void testNumberOfSentMessages() throws JMSException, JsonProcessingException {
        int numberOfMessages = 5;
        int numberOfConsumers = 2;
        producer.setNumberOfMessages(numberOfMessages);
        producer.setNumberOfConsumer(numberOfConsumers);
        producer.run();

        verify(messageProducer, times(numberOfMessages + numberOfConsumers))
                .send(session.createTextMessage(anyString()));
        verify(mapper, times(numberOfMessages)).writeValueAsString(any(MessagePojo.class));
    }

    @Test
    void testIfProducerStoppedByTimeout() {
        int numberOfMessages = 100000;
        producer.setNumberOfMessages(numberOfMessages);
        producer.run();

        assertTrue(producer.getCounterSentMessages() < numberOfMessages);
    }

    @Test
    void testThrowJsonProcessingExceptionIfSendMessage() throws JsonProcessingException {
        var messagePojo = mock(MessagePojo.class);
        when(mapper.writeValueAsString(messagePojo)).thenThrow(JsonProcessingException.class);

        assertThrows(ProducerFailException.class, () -> producer.sendMessage(session, messageProducer, messagePojo));
    }

    @Test
    void testThrowJMSExceptionIfSendMessage() throws JMSException, JsonProcessingException {
        var messagePojo = mock(MessagePojo.class);
        when(mapper.writeValueAsString(messagePojo)).thenReturn("message");
        when(session.createTextMessage(anyString())).thenThrow(JMSException.class);

        assertThrows(ProducerFailException.class, () -> producer.sendMessage(session, messageProducer, messagePojo));
    }

    @Test
    void testThrowJMSExceptionSendPoison() throws JMSException {
        when(session.createTextMessage("poison pill")).thenThrow(JMSException.class);
        assertThrows(ProducerFailException.class,
                () -> producer.sendPoison(session, messageProducer));
    }

    @Test
    void testCallSendPoison() throws JMSException {
        producer.setNumberOfConsumer(3);
        producer.sendPoison(session, messageProducer);

        verify(messageProducer, times(3)).send(session.createTextMessage("poison pill"));
    }
}