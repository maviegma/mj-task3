package com.mkravchenko;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MessagePojoGeneratorTest {
    private MessagePojoGenerator messagePojoGenerator;

    @BeforeAll
    void init(){
        messagePojoGenerator = new MessagePojoGenerator();
    }
    @Test
    void testCreateMessage() {
        var message = messagePojoGenerator.createMessage();
        assertTrue(message.matches(".*?"));

    }

    @Test
    void testCreateDate() {
        assertTrue(messagePojoGenerator.createDate().toString().matches("\\d{4}-\\d{2}-\\d{2}"));
    }

    @Test
    void testCreateEddr() {
        assertTrue(messagePojoGenerator.createEddr("456").matches("\\d{8}-0456\\d"));
    }

    @Test
    void testDateIsInRange() {
        var startDate = LocalDate.ofEpochDay(0);
        var endDate = LocalDate.now();
        var checkDate = messagePojoGenerator.createDate();

        assertTrue(!checkDate.isBefore(startDate) && !checkDate.isAfter(endDate));
    }

}