package com.mkravchenko;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mkravchenko.exeptions.ConsumerFailException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.commons.csv.CSVPrinter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.*;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConsumerTest {
    @Mock
    PooledConnectionFactory pooledConnectionFactory;
    @Mock
    private Connection connection;
    @Mock
    private Session session;
    @Mock
    Queue queue;
    @Mock
    MessageConsumer messageConsumer;
    @Mock
    TextMessage textMessage;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    Validator validator;
    @Mock
    CSVPrinter validPrinter;
    @Mock
    CSVPrinter invalidPrinter;
    @Mock
    MessagePojo messagePojo;
    @Mock
    Set<ConstraintViolation<MessagePojo>> constraintViolationSet;
    Consumer consumer;

    @BeforeEach
    void init() throws JMSException {
        lenient().when(pooledConnectionFactory.createConnection()).thenReturn(connection);
        lenient().when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        lenient().when(session.createQueue(anyString())).thenReturn(queue);
        lenient().when(session.createConsumer(any(Destination.class))).thenReturn(messageConsumer);
        lenient().when(messageConsumer.receive()).thenReturn(textMessage);
        consumer = new Consumer(pooledConnectionFactory, validPrinter, invalidPrinter, validator, objectMapper, "queue");
    }

    @Test
    void testNumberOfReceivedMessages() throws IOException, JMSException {
        prepareStubForExpectedNumberOfReceivedMessages();
        consumer.run();
        verify(validPrinter, times(2)).printRecord(anyInt(), anyString());
        verify(invalidPrinter, times(3))
                .printRecord(anyInt(), anyString(), any(ErrorContainer.class));
    }

    @Test
    void testArgumentForObjectMapper() throws JMSException, IOException {
        String message = prepareMessageForCheckArgumentOfObjectMapper();
        consumer.run();
        var captor = ArgumentCaptor.forClass(String.class);
        verify(objectMapper, times(1)).readValue(captor.capture(), eq(MessagePojo.class));
        Assertions.assertThat(captor.getValue())
                .isEqualTo(message);
    }

    private String prepareMessageForCheckArgumentOfObjectMapper() throws JMSException, JsonProcessingException {
        var message = new MessagePojoGenerator().toString();
        when(textMessage.getText())
                .thenReturn(message)
                .thenReturn("poison pill");
        when(objectMapper.readValue(message, MessagePojo.class)).thenReturn(messagePojo);
        return message;
    }

    @Test
    void testThrowJsonProcessingExceptionSendMessage() throws JsonProcessingException {
        when(objectMapper.readValue("badMessage", MessagePojo.class)).thenThrow(JsonProcessingException.class);

        assertThrows(ConsumerFailException.class, () -> consumer.createMessagePojo("badMessage"));
    }

    @Test
    void testThrowJMSExceptionIfGetTextFromMessage() throws JMSException {
        var mockMessage = mock(TextMessage.class);
        when((mockMessage).getText()).thenThrow(JMSException.class);

        assertThrows(ConsumerFailException.class,
                () -> consumer.getTextFromMessage(mockMessage));
    }

    private void prepareStubForExpectedNumberOfReceivedMessages() throws JMSException, JsonProcessingException {
        when(textMessage.getText())
                .thenReturn("message")
                .thenReturn("message")
                .thenReturn("message")
                .thenReturn("message")
                .thenReturn("message")
                .thenReturn("poison pill");
        when(objectMapper.readValue("message", MessagePojo.class)).thenReturn(messagePojo);
        when(messagePojo.getCount()).thenReturn(1);
        when(messagePojo.getName()).thenReturn("message");
        when(constraintViolationSet.isEmpty()).thenReturn(false);
        when(validator.validate(any(MessagePojo.class)))
                .thenReturn(constraintViolationSet)
                .thenReturn(Collections.emptySet())
                .thenReturn(constraintViolationSet)
                .thenReturn(Collections.emptySet())
                .thenReturn(constraintViolationSet);
    }


}