package com.mkravchenko.util;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Properties;


class PropertiesReaderTest {

    @Test
    void test() {
        var propertiesReader = new PropertiesReader(new Properties());

        Assertions.assertThatThrownBy(() -> propertiesReader.get("invalidProperty"))
                .isInstanceOf(IllegalArgumentException.class);
    }
}