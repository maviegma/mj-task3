package com.mkravchenko.util;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class PropertiesLoaderTest {

    @Test
    void testThrowExceptionIfFileNotFound1() {
        var loader = new PropertiesLoader();
        assertThatThrownBy(() -> loader.loadProperties("file"))
                .isInstanceOf(RuntimeException.class);
    }

}