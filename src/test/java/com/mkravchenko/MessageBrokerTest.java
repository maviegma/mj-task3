package com.mkravchenko;

import com.mkravchenko.util.PropertiesLoader;
import com.mkravchenko.util.PropertiesReader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MessageBrokerTest {
    PropertiesReader propertiesReader;

    @BeforeAll
    public void load() {
        propertiesReader = new PropertiesReader(new PropertiesLoader().loadProperties("app.properties"));
    }

    @Test
    void testNumberOfRunningProcesses() {
        var messageBroker = new MessageBroker(10, propertiesReader);
        var numberOfProducers = Integer.parseInt(propertiesReader.get("numberOfProducer"));
        var numberOfConsumers = Integer.parseInt(propertiesReader.get("numberOfConsumer"));
        messageBroker.run();

        assertThat(messageBroker.getRunningProducerCounter()).isEqualTo(numberOfProducers);
        assertThat(messageBroker.getRunningConsumerCounter()).isEqualTo(numberOfConsumers);
    }
}